package Templates;

public class InsertionSort {  
    public static void insertionSort(int array[]) {  
        int n = array.length;  
        int count = 0;
        for (int j = 1; j < n; j++) {  
            int key = array[j];  
            int i = j-1;  
            while ( (i > -1) && ( array [i] > key ) ) {  
                array [i+1] = array [i];  
                i--;  
                count++;
            }  
            array[i+1] = key;  
        }  
        System.out.println("shifting sebanyak " + count);
    }  
    
    public static void main(String a[]){    
        int[] arr1 = {10,9,8,7,6,5,4,3,2,1};    
        System.out.println("Before Insertion Sort");    
        for(int i:arr1){    
            System.out.print(i+" ");    
        }    
        System.out.println();    
            
        insertionSort(arr1);//sorting array using insertion sort    
        
        System.out.println("After Insertion Sort");    
        for(int i:arr1){    
            System.out.print(i+" ");    
        }    
    }    
}    