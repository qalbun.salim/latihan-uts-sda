
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal2A {
    private static InputReader in;
    private static PrintWriter out;
    private static int[] arr1;
    private static int[] arr2;
    private static int[] arr3;
    private static int[] res;
    
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        
        int N1 = in.nextInt();
        int N2 = in.nextInt();
        int N3 = in.nextInt();

        arr1 = new int[N1];
        arr2 = new int[N2];
        arr3 = new int[N3];
        res = new int[N1+N2+N3];
        

        for (int i=0; i<N1; i++) {
            arr1[i] = in.nextInt();
        }

        for (int i=0; i<N2; i++) {
            arr2[i] = in.nextInt();

        }

        for (int i=0; i<N3; i++) {
            arr3[i] = in.nextInt();
        }

        int[] hasil = new int[N1+N2+N3];
        hasil = merging3(arr1,arr2,arr3);

        sort(hasil, 0, N1+N2+N3);
        printArray(hasil);


        out.flush();
    }

    static int[] merging3(int[] arr1, int[] arr2, int[] arr3) {

        int index = 0;
        for (int i=0; i<arr1.length; i++) {
            res[index++] = arr1[i];
        }
        for (int i=0; i<arr2.length; i++) {
            res[index++] = arr2[i];
        }
        for (int i=0; i<arr3.length; i++) {
            res[index++] = arr3[i];
        }

        return res;
    }

    static void merge(int arr[], int l, int m, int r)
    {
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;
 
        /* Create temp arrays */
        int L[] = new int[n1];
        int R[] = new int[n2];
 
        /*Copy data to temp arrays*/
        for (int i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];
 
        /* Merge the temp arrays */
 
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;
 
        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }
 
        /* Copy remaining elements of R[] if any */
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
 
    // Main function that sorts arr[l..r] using
    // merge()
    static void sort(int arr[], int l, int r)
    {
        if (l < r) {
            // Find the middle point
            int m = (l + r) / 2;
 
            // Sort first and second halves
            sort(arr, l, m);
            sort(arr, m + 1, r);
 
            // Merge the sorted halves
            merge(arr, l, m, r);
        }
    }

    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            out.print(arr[i] + " ");
        out.println();
    }
    
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }

}