package Templates;

import java.util.Random;

class Node {
  int data;
  Node next, prev;

  public Node(int data) {
      this.data = data;
  }
}

class DLList {
  Node header;

  void cetak(int sum){

    Node last = header;
    Node head = header;

    while(last.next.data < sum) {
        last = last.next;
    }

    while(head != last) {
      int temp = head.data + last.data;
      if (temp == sum) {
        System.out.println("("+ head.data + " " + last.data+")");
        head = head.next;
        last = last.prev;
      } 
      if (temp > sum) last = last.prev;
      else head = head.next;
    }
  }
}

public class NodeTest {

  public static void main(String[] args) {
    
    DLList list = new DLList();
    // Random random = new Random();

    list.header = new Node(0);
    Node node = list.header;
    Node prev = node;

    for (int i = 1; i <= 100; i++) {
      node.next = new Node(i);
      node = node.next;
      node.prev = prev;
      prev = node;
    }
    node = list.header;

    list.cetak(90);
  }
}