// package Templates;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal1B {

    static void merge(String arr[], int l, int m, int r)
    {
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;
 
        /* Create temp arrays */
        String[] L = new String[n1];
        String[] R = new String[n2];
 
        /*Copy data to temp arrays*/
        for (int i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];
 
        /* Merge the temp arrays */
 
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;
 
        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2) {
            if (L[i].compareTo(R[j]) < 0) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }
 
        /* Copy remaining elements of R[] if any */
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
 
    // Main function that sorts arr[l..r] using
    // merge()
    static void sort(String arr[], int l, int r)
    {
        if (l < r) {
            // Find the middle point
            int m = (l + r) / 2;
 
            // Sort first and second halves
            sort(arr, l, m);
            sort(arr, m + 1, r);
 
            // Merge the sorted halves
            merge(arr, l, m, r);
        }
    }
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        InputReader in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        PrintWriter out = new PrintWriter(outputStream);
        Set<String> buahSet = new HashSet<>(); 


        int anggotaA = in.nextInt();
        int anggotaB = in.nextInt();

        String[] kelompokA = new String[anggotaA];
        String[] kelompokB = new String[anggotaB];

        int totalC = anggotaA + anggotaB;

        for (int i=0; i<anggotaA; i++) {
            String buah = in.next();
            if (buahSet.contains(buah)){
                continue;
            }
            kelompokA[i] = buah;
            buahSet.add(buah);
        }
        for (int i=0; i<anggotaB; i++) {
            String buah = in.next();
            if (buahSet.contains(buah)){
                continue;
            }
            kelompokB[i] = buah;
            buahSet.add(buah);
        }
        String[] kelompokC = new String[buahSet.size()];
        int pointer = 0;
        for (String i : buahSet){
            kelompokC[pointer++] = i;
        }
        String cariKata = in.next();
        sort(kelompokC,0,kelompokC.length-1);
        out.println(Arrays.toString(kelompokC));
        
        for (String buah : kelompokC){
            if (buah.equals(cariKata)){
                out.println(buah);
                break;
            }
        }

        out.flush();
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}
