

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal1A {
    private static InputReader in;
    private static PrintWriter out;
    private static ArrayList<String> kumpulan;

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        kumpulan = new ArrayList<>();
        int banyak = in.nextInt()+in.nextInt();

        for (int i=0; i<banyak; i++) {
            kumpulan.add(in.next());
        }
        Collections.sort(kumpulan);

        ArrayList<String> newList = new ArrayList<>();

        newList.add(kumpulan.get(0));

        for (int i = 1; i < kumpulan.size(); i++) {
            String last = newList.get(newList.size()-1);
            String now = kumpulan.get(i);
            if (!last.equals(now)) newList.add(now);
        }
        String findString = in.next();
        for (int i = 0; i < newList.size(); i++) {
            if (findString.equals(newList.get(i))) {
                out.println(i);
                out.close();
                return;
            }
        }
        out.println(-1);
        out.close();
    }

    
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }

}