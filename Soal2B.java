
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal2B {
    private static InputReader in;
    private static PrintWriter out;
    
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int N1 = in.nextInt();
        int N2 = in.nextInt();
        int N3 = in.nextInt();

        int[] arr1 = new int[N1];
        int[] arr2 = new int[N2];
        int[] arr3 = new int[N3];
        

        for (int i=0; i<N1; i++) {
            arr1[i] = in.nextInt();
        }

        for (int i=0; i<N2; i++) {
            arr2[i] = in.nextInt();

        }

        for (int i=0; i<N3; i++) {
            arr3[i] = in.nextInt();
        }

        out.println(Arrays.toString(merging3(arr1,arr2,arr3)));

        out.close();
    }

    static int[] merging3(int[] arr1, int[] arr2, int[] arr3) {
        Queue<Integer> list1 = new LinkedList<>();
        Queue<Integer> list2 = new LinkedList<>();
        Queue<Integer> list3 = new LinkedList<>();
        Queue<Integer> temp = new LinkedList<>();
        int[] result = new int[arr1.length+arr2.length+arr3.length];
        int index = 0;

        for (int i=0; i<arr1.length;i++) {
            list1.add(arr1[i]);
        }
        for (int i=0; i<arr2.length;i++) {
            list2.add(arr2[i]);
        }
        for (int i=0; i<arr3.length;i++) {
            list3.add(arr3[i]);
        }

        while(!list1.isEmpty() && !list2.isEmpty()) {
            if(list1.peek() < list2.peek()) {
                temp.add(list1.poll());
            } else temp.add(list2.poll());
        }
        while(!list1.isEmpty()) temp.add(list1.poll());
        while(!list2.isEmpty()) temp.add(list2.poll());

        while(!list3.isEmpty() && !temp.isEmpty()) {
            if(list3.peek() < temp.peek()) {
                result[index++] = list3.poll();
            } else result[index++] = temp.poll();
        }
        while(!list3.isEmpty()) result[index++] = list3.poll();
        while(!temp.isEmpty()) result[index++] = temp.poll();

        return result;
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }

}