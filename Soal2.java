package Templates;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal2 {
    private static InputReader in;
    private static PrintWriter out;
    private static int[] arr1;
    private static int[] arr2;
    private static int[] arr3;
    private static int[] res;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        int N1 = in.nextInt();
        int N2 = in.nextInt();
        int N3 = in.nextInt();

        arr1 = new int[N1];
        arr2 = new int[N2];
        arr3 = new int[N3];
        

        for (int i=0; i<N1; i++) {
            arr1[i] = in.nextInt();
        }

        for (int i=0; i<N2; i++) {
            arr2[i] = in.nextInt();

        }

        for (int i=0; i<N3; i++) {
            arr3[i] = in.nextInt();
        }

        out.println(Arrays.toString(merging3(arr1, arr2, arr3)));
    }

    static int[] merging3(int[] arr1, int[] arr2, int[] arr3) {
        int indexRes = 0;
        int pointer1 = 0;
        int pointer2 = 0;
        int pointer3 = 0;

        res = new int[arr1.length+arr2.length+arr3.length];
        
        for (int i=0; i<res.length; i++) {

        
            if (pointer1 < arr1.length) {
                if (pointer2 < arr2.length && arr1[pointer1] <= arr2[pointer2]){
                    if (pointer3 < arr3.length && arr1[pointer1] <= arr3[pointer3]){
                        res[indexRes++] = arr1[pointer1];
                    }
                    else{
                        res[indexRes++] = arr3[pointer3];
                    }
                } else{
                    res[indexRes++] = arr2[pointer2];
                }

                pointer1++;
            }

            else if (pointer2 < arr2.length) {
                if (pointer1 < arr1.length && arr2[pointer2] <= arr1[pointer1]){
                    if (pointer3 < arr3.length && arr2[pointer2] <= arr3[pointer3]){
                        res[indexRes++] = arr2[pointer2];
                    }
                    else{
                        res[indexRes++] = arr3[pointer3];
                    }
                } else{
                    res[indexRes++] = arr1[pointer2];
                }

                pointer1++;
            }
        }
        break;


        }

        return res;

    }



    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}