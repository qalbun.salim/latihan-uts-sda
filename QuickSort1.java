package Templates;

public class QuickSort1 {
    public static void main(String[] args) {
        int [] bilangan = new int []{40,2,1,43,3,65,0,-1,58,3,42,4};
        cetak (bilangan);
        quicksort(bilangan, 0, bilangan.length-1);
    }
    static void cetak (int[] arr) {
        for (int i= 0; i<= arr.length-2; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println(arr[arr.length-1]);
    }

    static void quicksort (int[] arr, int left, int right) {
        if (left>right) // base case kalo sudah crossing
            return;

        int OriginalLeft = left;
        int OriginalRigt = right;
        int pivot = arr [left];
        int pivotFlag = 0; // Pivot sedang ada di kiri
        
        while (left!=right) {
            if (pivotFlag == 0) {
                //bandingin sama yang kanan
                if (pivot<arr[right]) { // gak perlu di swap
                    // geser right ke kiri
                    right--;
                }
                else if (pivot >= arr[right]) {
                    arr[left] = arr[right];
                    arr[right] = pivot;
                    
                    left++; // left sudah benar jadi geser ke kanan
                    pivotFlag = 1; // pivot jadi disebelah kanan
    
                }
            }
            else { // pivot di sebelah kanan
    
                if (pivot>arr[left]) { // gak perlu di swap
                    // geser left ke kanan
                    left++;
                }
                else if (pivot <= arr[left]) {
                    arr[right] = arr[left];
                    arr[left] = pivot;
                    
                    right--; // left sudah benar jadi geser ke kanan
                    pivotFlag = 0; // pivot jadi disebelah kanan
    
                }
            }
            cetak (arr);
        }
        
        // pivot sudah ketemu jadi tinggal rekursif balik lagi 
        // ke quick sort

        // INGAT PIVOT TIDAK IKUT
        quicksort(arr, OriginalLeft, left-1); // sebelah kiri
        quicksort(arr, left+1, OriginalRigt); // sebelah kanan

    }
        
    
}