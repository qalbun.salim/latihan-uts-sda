package Templates;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

class MyNode{
    MyNode next;
    int data;
    public MyNode(int data){
        this.data = data;
    }
    public MyNode(){}
}
public class MyLinkedList {
    MyNode pointer;
    MyNode nullHeader;
    public MyLinkedList(){
        nullHeader = new MyNode();
        pointer = nullHeader;
    }
    
    public void add(MyNode node){
        if (pointer == nullHeader){
            nullHeader.next = node;
            pointer = node;
        } else{
            pointer.next = node;
            pointer = pointer.next;
        }
    }

    public MyLinkedList myFunc(MyLinkedList other){
        MyLinkedList diff = new MyLinkedList();
        MyNode otherPointer = other.nullHeader.next;
        MyNode myPointer = this.nullHeader.next;
        while (myPointer.data != 0){
            if (otherPointer.data != 0){
                if (myPointer.data > otherPointer.data){
                    otherPointer = otherPointer.next;
                    continue;
                }
                else if (myPointer.data == otherPointer.data){
                    myPointer = myPointer.next;
                    otherPointer = otherPointer.next;
                } else {
                    diff.add(myPointer);
                    myPointer  = myPointer.next;
                }
            } else {
                if (myPointer.data != 0){
                    
                }
                diff.add(myPointer);
                myPointer = myPointer.next;
            }
            
        }
        return diff;
    }
    
    public String toString(){
        MyNode mypointer = nullHeader.next;
        String a = "";
        while (mypointer != null){
            a += String.format("%s ",mypointer.data);
            mypointer = mypointer.next;
        }
        return a;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        MyLinkedList a = new MyLinkedList();
        MyLinkedList b = new MyLinkedList();
        for (int i = 0; i < 2; i++){
            String line = in.nextLine();
            Scanner in2 = new Scanner(line);
            while (in2.hasNextInt()){
                if (i == 0){
                    a.add(new MyNode(in2.nextInt())); 
                } else{
                    b.add(new MyNode(in2.nextInt()));
                }
                
            }
            in2.close();
        }
        System.out.println(a.myFunc(b));
        in.close();

    }
}

// taken from https://codeforces.com/submissions/Petr
// together with PrintWriter, these input-output (IO) is much faster than the usual Scanner(System.in) and System.out
// please use these classes to avoid your fast algorithm gets Time Limit Exceeded caused by slow input-output (IO)
class InputReader {
    public BufferedReader reader;
    public StringTokenizer tokenizer;

    public InputReader(InputStream stream) {
        reader = new BufferedReader(new InputStreamReader(stream), 32768);
        tokenizer = null;
    }

    public String next() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }

}
