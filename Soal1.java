package Templates;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class Soal1 {
    private static InputReader in;
    private static PrintWriter out;
    private static List<String> kelompokA;
    private static List<String> kelompokB;
    static int anggotaA;
    static int anggotaB;


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        kelompokA = new ArrayList<>();
        kelompokB = new ArrayList<>();

        anggotaA = in.nextInt();
        anggotaB = in.nextInt();

        for (int i=0; i<anggotaA; i++) {
            kelompokA.add(in.next());
        }
        for (int i=0; i<anggotaB; i++) {
            kelompokB.add(in.next());
        }
        
        int totalC = anggotaA + anggotaB;

        String anggota = in.next();
        out.println(searchByIndex(anggota, totalC));

        out.flush();

    }

    static int searchByIndex(String anggota, int totalC) {

        List<String> kelompokC = new ArrayList<>();
        for (int i=0; i<totalC; i++) {
            if (i < anggotaA) {
                if (!kelompokC.contains(kelompokA.get(i))) {
                    kelompokC.add(kelompokA.get(i));
                }
            } else {
                int indexB = i-anggotaA;
                if (!kelompokC.contains(kelompokB.get(indexB))) {
                    kelompokC.add(kelompokB.get(indexB));
                }
            }
        }

        

        if (kelompokC.contains(anggota)) {
            return kelompokC.indexOf(anggota);
        }

        return -1;
    }
    
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }

}
